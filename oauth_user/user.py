from .models import UserCustom, Grants


class UserTrait:
    def __init__(self, user):
        self.user = user

    def valid_grant_url(self, url):
        find = False
        grants = self.user.grants.all()
        for grant in grants:
            if grant.url == url:
                find = True
        return find
