from django import forms


class UserForm(forms.Form):
    id = forms.IntegerField(required=True, min_value=1)
    username = forms.CharField(required=True, min_length=5, max_length=15)
    url = forms.URLField(required=True, min_length=1)
