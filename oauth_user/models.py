from django.db import models

# Create your models here.
from django.db import models


class Grants(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField(null=True)

    class Meta:
        ordering = ("name",)


class UserCustom(models.Model):
    username = models.CharField(max_length=50)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    grants = models.ManyToManyField(Grants)

    class Meta:
        ordering = ("name",)
