import unittest
from oauth_user.models import UserCustom, Grants
from .oauth import OauthTrait
import json


class TestStrings(unittest.TestCase):
    def setUp(self):

        self.current_user = UserCustom.objects.create(
            name="Victor", username="victorlt", is_active=True
        )

        grant_01 = Grants.objects.create(name="grant_01", url="google.com")

        self.current_user.grants.add(grant_01)

        grant_02 = Grants.objects.create(name="grant_02", url="youtube.com")

        self.current_user.grants.add(grant_02)

        # self.current_user = current_user

    def test_id_no_valid(self):
        oauth = OauthTrait
        data_send = {"id": "jfhfhfh", "username": "victorlt", "url": "google.com"}
        response = oauth.valid_request(data_send)
        self.assertEqual(str(response.content, encoding="utf8"), '{"response": "FAIL"}')

    def test_invalid_user_request(self):
        oauth = OauthTrait
        data_send = {"id": 1, "username": "victorlts", "url": "google.com"}
        response = oauth.valid_request(data_send)
        self.assertEqual(
            str(response.content, encoding="utf8"),
            '{"response": "El usuario no fue encontrado o esta inactivo", "messages": {}}',
        )

    def test_username_not_registered(self):
        oauth = OauthTrait
        id = 1
        data_send = {"id": id, "username": "victorlt", "url": "google.com"}
        response = oauth.valid_request(data_send)
        self.assertEqual(
            str(response.content, encoding="utf8"),
            '{"response": "FAIL"}',
        )

    def test_valid_user(self):
        oauth = OauthTrait
        data_send = {
            "id": self.current_user.id,
            "username": "victorlt",
            "url": "google.com",
        }
        response = oauth.valid_request(data_send)
        self.assertEqual(
            str(response.content, encoding="utf8"),
            '{"username": "victorlt", "name": "Victor", "is_active": true, "grants": [{"name": "grant_01"}, {"name": "grant_02"}]}',
        )

    def test_invalid_url(self):
        oauth = OauthTrait
        data_send = {"id": self.current_user.id, "username": "victorlt", "url": 1}
        response = oauth.valid_request(data_send)
        self.assertEqual(str(response.content, encoding="utf8"), '{"response": "FAIL"}')
