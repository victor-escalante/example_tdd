from .models import UserCustom
from .form import UserForm
from django.http import JsonResponse
from .user import UserTrait
from .serializer import UserSerializer


class OauthTrait:
    def valid_request(data):

        user_form = UserForm(data=data)
        if user_form.is_valid():

            user = UserCustom.objects.filter(
                pk=data["id"], username=data["username"], is_active=True
            ).first()

            if not user:
                return JsonResponse(
                    {
                        "response": "El usuario no fue encontrado o esta inactivo",
                        "messages": user_form.errors,
                    }
                )

            user_trait = UserTrait(user)
            valid = user_trait.valid_grant_url(data["url"])

            if valid:
                return JsonResponse(UserSerializer(user).data)

        return JsonResponse({"response": "FAIL"})
