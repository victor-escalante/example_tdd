# Generated by Django 2.2.1 on 2019-05-28 08:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("oauth_user", "0001_initial")]

    operations = [
        migrations.AddField(
            model_name="grants", name="url", field=models.URLField(null=True)
        )
    ]
