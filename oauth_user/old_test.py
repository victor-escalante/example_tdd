from django.test import TestCase, Client
from oauth_user.models import UserCustom, Grants
from .oauth import OauthTrait


class LoginTest(TestCase):
    def setUp(self):

        current_user = UserCustom.objects.create(
            name="Victor", username="victorlt", is_active=True
        )

        grant_01 = Grants.objects.create(name="grant_01", url="google.com")

        current_user.grants.add(grant_01)

        grant_02 = Grants.objects.create(name="grant_02", url="youtube.com")

        current_user.grants.add(grant_02)

        self.current_user = current_user

    def test_check_current_user(self):
        user = self.current_user
        self.assertEqual(user.name, "Victor")

    def test_user_is_active(self):
        user = self.current_user
        self.assertEqual(user.is_active, True)

    def test_valid_user_request(self):
        oauth = OauthTrait
        response = oauth.valid_request(1, "victorlt", "google.com")
        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            {
                "username": "victorlt",
                "is_active": True,
                "name": "Victor",
                "grants": [{"name": "grant_01"}, {"name": "grant_02"}],
            },
        )

    def test_id_no_valid(self):
        oauth = OauthTrait
        response = oauth.valid_request("jfhfhfh", "victorlt", "google.com")
        self.assertJSONEqual(
            str(response.content, encoding="utf8"), {"response": "FAIL"}
        )

    def test_username_not_registered(self):
        oauth = OauthTrait
        response = oauth.valid_request(1, "victor", "google.com")
        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            {
                "messages": {},
                "response": "El usuario no fue encontrado o esta inactivo",
            },
        )

    def test_invalid_url(self):
        oauth = OauthTrait
        response = oauth.valid_request(1, "victorlt", "googler.com")
        self.assertJSONEqual(
            str(response.content, encoding="utf8"), {"response": "FAIL"}
        )
