from rest_framework import routers, serializers, viewsets


class GrantSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=50)


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=50)
    name = serializers.CharField(max_length=255)
    is_active = serializers.BooleanField(default=True)
    grants = GrantSerializer(many=True)
