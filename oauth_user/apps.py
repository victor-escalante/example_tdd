from django.apps import AppConfig


class OauthUserConfig(AppConfig):
    name = "oauth_user"
